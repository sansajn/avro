TARGET = timer1_pwm
SOURCES = timer1_pwm.cpp
MCU = atmega8
CPU_FREQ = 1000000
PROGRAMMER = stk500v2
PORT = /dev/ttyUSB0

CXX_STANDARD = c++11

CXXFLAGS = -Wall -O2 -g -mmcu=$(MCU) -DF_CPU=$(CPU_FREQ)UL

CXX = avr-g++
OBJCOPY = avr-objcopy
OBJDUMP = avr-objdump

OBJECTS = $(SOURCES:.cpp=.o)

all: clean $(SOURCES) $(TARGET) text list

clean:
	-rm -f $(TARGET).hex $(TARGET) *.o

flash: $(TARGET).hex
	avrdude -c $(PROGRAMMER) -p $(MCU) -P $(PORT) -e -U flash:w:$<

list: $(TARGET)
	$(OBJDUMP) -h -S $(TARGET) > $(TARGET).lss


text:
	$(OBJCOPY) -j .text -j .data -O ihex $(TARGET) $(TARGET).hex

_CXXFLAGS = $(CXXFLAGS) -std=$(CXX_STANDARD)

$(TARGET): $(OBJECTS)
	$(CXX) $(_CXXFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CXX) -c $(_CXXFLAGS) $< -o $@
