// tri pripojene ledky, kde prva svieti na 15% druha 50% a tretia 100% vykonu
#include <avr/io.h>
#include "timer.hpp"

using namespace avro;


int main()
{
	// connect led to PB1, PB2 and PB5 pin
	DDRB |= (1 << PB1)|(1 << PB2)|(1 << PB3);

	// 2 channel, 8 bit fast pwm
	timer1_fast_pwm pwm(220, 127, timer1_clock_select::clk8, timer1_fast_pwm::clear_on_match, timer1_fast_pwm::clear_on_match);

	PORTB &= ~(1 << PB3);  // 100%

	while (1)
	{}

	return 0;
}
