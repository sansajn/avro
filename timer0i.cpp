// pouzitie 8-bit casovaca 0 s prerusenim
#include "timer.hpp"
#include <avr/interrupt.h>

using namespace avro;

volatile uint8_t overflow_count = 0;  // jedno pretecenie je 2^8 * 1/F_CPU * prescaler sekund

ISR(TIMER0_OVF_vect)
{
	overflow_count += 1;
}

int main(void)
{
	DDRC |= (1 << 0);  // connect led to pin PC0

	timer0 t0(clk_io8);
	t0.enable_overflow_interrupt();

	sei();  // enable global interrupt flag

	uint8_t overflow_compare = 98;

	while (1)
	{
		if (overflow_count >= overflow_compare)
		{
			PORTC ^= (1 << 0);
			overflow_count = 0;
		}
	}

	return 0;
}
