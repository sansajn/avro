// pouzitie 16-bit casovaca 1 s prerusenim v CTC rezime
#include <avr/io.h>
#include <avr/interrupt.h>
#include "timer.hpp"

using namespace avro;

ISR(TIMER1_COMPA_vect)
{
	PORTC ^= (1 << 0);
}

int main(void)
{
	DDRC |= (1 << 0);  // connect led to pin PC0

	timer1_ctc ctc(25000, timer1_clock_select::clk8, timer1_ctc::disconnected);
	ctc.enable_compare_interrupt();
	sei();  // enable global interrupts

	while (1)
	{}

	return 0;
}
