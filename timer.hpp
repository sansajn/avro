// designed for ATmega8
#pragma once
#include <avr/io.h>

namespace avro {

enum timer_clock_source {  //!< table 34. clock select bit description
	stopped,
	clk_io,
	clk_io8,
	clk_io64,
	clk_io256,
	clk_io1024,
	fall_t0,
	rise_t0
};

enum timer_mode {  //!< table 42. waveform generation mode bit description
	normal,
	pwm,
	ctc,
	fast_pwm,
};



template <typename Trait>
struct timer8  //!< Abstrakcia 8bit timer/counter hw (pouzitelna pre timer0 a timer2).
{
	volatile uint8_t & value;

	timer8() : value(sfr8(Trait::tcnt_addr)) {}

	timer8(timer_clock_source c) : value(sfr8(Trait::tcnt_addr))
	{
		clock(c);
		value = 0;
	}

	void clock(timer_clock_source c) const {sfr8(Trait::tccr_addr) |= c;}
	void mode(timer_mode m) const;

	void enable_overflow_interrupt() const {TIMSK |= (1 << Trait::toie);}
	void enable_compare_match_interrupt() const {TIMSK |= (1 << Trait::ocie);}
};

struct timer0_trait
{
	static uint8_t const ocr_addr = 0xFF;  // undefined for atmega8
	static uint8_t const tcnt_addr = 0x32;
	static uint8_t const tccr_addr = 0x33;
	static uint8_t const toie = 0;  //!< overflow interrupt mask offset
	static uint8_t const ocie = 1;  // undefined for atmega8
};

struct timer2_trait
{
	static uint8_t const ocr_addr = 0x23;
	static uint8_t const tcnt_addr = 0x24;
	static uint8_t const tccr_addr = 0x25;
	static uint8_t const toie = 6;  //!< overflow interrupt mask offset
	static uint8_t const ocie = 7;  //!< compare interrupt mask offset
};

using timer0 = timer8<timer0_trait>;
using timer2 = timer8<timer0_trait>;



enum class timer1_clock_select {  // table 40. clock select bit description
	stopped,
	clk,
	clk8,
	clk64,
	clk256,
	clk1024,
	fall_t1,
	rise_t1,
};

inline void disable_timer1_interrupts()
{
	TIMSK &= ~((1 << TICIE1)|(1 << OCIE1A)|(1 << OCIE1B)|(1 << TOIE1));
}

struct timer1  //!< timer1 in owerflow interrupt mode
{
	volatile uint16_t & value;

	timer1(timer1_clock_select c)	: value(TCNT1)
	{
		disable_timer1_interrupts();
		TCCR1A = 0;
		TCCR1B = uint8_t(c);
		value = 0;
		enable_overflow_interrupt();
	}

	void enable_overflow_interrupt() const {TIMSK |= (1 << TOIE1);}
};

/*! timer1 CTC (Clear Timer on Compare) wrapper
\code
timer1 ctc_intr(25000, timer1_clock_select::clk8);  // timer1 in CTC with compare interrupt
timer1 ctc_oc1a(25000, timer1_clock_select::clk8, timer1_ctc::toggle_on_match);  // timer1 in CTC with OC1A output

timer1 ctc_mix(25000, timer1_clock_select::clk8, timer1_ctc::toggle_on_match);
ctc_mix.enable_compare_interrupt();  // timer1 in CTC with both OC1A output and compare interrupt
\endcode */
struct timer1_ctc
{
	enum output_mode {
		disconnected,
		toggle_on_match,
		clear_on_match,
		set_on_match
	};

	volatile uint16_t & compare_value;

	timer1_ctc(uint16_t compare, timer1_clock_select c)  //!< timer1 in CTC with compare interrupt
		: timer1_ctc(compare, c, output_mode::disconnected)
	{
		enable_compare_interrupt();
	}

	timer1_ctc(uint16_t compare, timer1_clock_select c, output_mode m)
		: compare_value(OCR1A)
	{
		disable_timer1_interrupts();
		TCCR1A = (m << 6);
		TCCR1B = (1 << WGM12) | uint8_t(c);
		TCNT1 = 0;
		compare_value = compare;
	}

	void enable_compare_interrupt() const {TIMSK |= (1 << OCIE1A);}
};

struct timer1_fast_pwm  //!< 8 bit fast pwm
{
	enum output_mode {
		disconnected,
		toggle_on_match,
		clear_on_match,
		set_on_match
	};

	volatile uint16_t & compare_a;
	volatile uint16_t & compare_b;

	timer1_fast_pwm(uint8_t compare, timer1_clock_select c, output_mode m)  //!< 1 channel, 8 bit fast pwm
		: compare_a(OCR1A), compare_b(OCR1B)
	{
		disable_timer1_interrupts();		
		TCCR1A = (1 << WGM10) | (m << COM1A0);
		TCCR1B = (1 << WGM12) | uint8_t(c);  // 8-bit fast pwm
		TCNT1 = 0;
		compare_a = compare;
	}

	//! two channel 8-bit fast pwm
	timer1_fast_pwm(uint8_t compa, uint8_t compb, timer1_clock_select c, output_mode a, output_mode b)
		: compare_a(OCR1A), compare_b(OCR1B)
	{
		disable_timer1_interrupts();
		TCCR1A = (1 << WGM10) | (a << COM1A0)|(b << COM1B0);
		TCCR1B = (1 << WGM12) | uint8_t(c);  // 8-bit fast pwm
		TCNT1 = 0;
		compare_a = compa;
		compare_b = compb;
	}
};

}  // avro
