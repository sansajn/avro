// pouzitie 16-bit casovaca 1 s prerusenim
#include <avr/io.h>
#include <avr/interrupt.h>
#include "timer.hpp"

using namespace avro;

volatile uint8_t overflow_count = 0;

ISR(TIMER1_OVF_vect)
{
	overflow_count += 1;

	if (overflow_count >= 31)  // ~2s
	{
		PORTC ^= (1 << 0);
		overflow_count = 0;
	}
}

int main(void)
{
	DDRC |= (1 << 0);  // connect led to pin PC0

	timer1 t1(timer1_clock_select::clk);
	sei();  // enable global interrupts

	while (1)
	{}

	return 0;
}
