// pouzitie 16-bit casovaca 1 v CTC rezime s vyuzitim OC1A pinu a compare prerusenia ako vystupu
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "timer.hpp"

using namespace avro;

ISR(TIMER1_COMPA_vect)
{
	PORTC ^= (1 << PC0);
}

int main(void)
{
	DDRB |= (1 << PB1);  // connect led to pin PB1(OC1A)
	DDRC |= (1 << PC0);  // connect led to pin PC0

	while (1)
	{
		timer1_ctc ctc_oc1a(25000, timer1_clock_select::clk8, timer1_ctc::toggle_on_match);

		_delay_ms(5000);

		timer1_ctc ctc_intr(25000, timer1_clock_select::clk8);
		sei();  // enable global interrupt

		_delay_ms(5000);
	}

	return 0;
}

