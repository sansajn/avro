// pouzitie 16-bit casovaca 1 v CTC rezime s vyuzitim OC1A pinu ako vystupu
#include "timer.hpp"

using namespace avro;


int main(void)
{
	DDRB |= (1 << 1);  // connect led to pin PB1(OC1A)

	timer1_ctc ctc1(25000, timer1_clock_select::clk8, timer1_ctc::toggle_on_match);

	while (1)
	{}

	return 0;
}
