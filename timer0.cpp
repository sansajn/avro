// pouzitie 8-bit casovaca 0 bez prerusenia s delickou
#include "timer.hpp"

using namespace avro;

int main()
{
	DDRC |= (1 << 0);  // pin PC0 as led output

	timer8 t0;
	t0.clock(clk_io1024);
	t0.value = 0;

	uint8_t compare_val = 195;  // T_out 0.1997s

	while (1)
	{
		if (t0.value >= compare_val)
		{
			PORTC ^= (1 << 0);
			t0.value = 0;
		}
	}

	return 0;
}
