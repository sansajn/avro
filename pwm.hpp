#pragma once
#include <avr/io.h>

namespace avro {

#define sfr8(adr) (*(uint8_t *)(adr))

enum class timer_clock_select {
	stopped,
	clk,
	clk8,
	clk32,
	clk64,
	clk128,
	clk256,
	clk1024  // clk/1024 (from prescaler)
};

enum class fast_pwm_output_mode
{
	disconnected = 0,
	clear_on_match = 2,
	set_on_match = 3
};

template <typename Traits>
struct basic_fast_pwm
{
	volatile uint8_t & compare_value;

	basic_fast_pwm(timer_clock_select c = timer_clock_select::clk, fast_pwm_output_mode o = fast_pwm_output_mode::set_on_match)
		: compare_value(sfr8(Traits::ocr_addr))
	{
		sfr8(Traits::tccr_addr) = (1 << Traits::wgm1)|(1 << Traits::wgm0) |
			(((uint8_t(o) >> 1)&1) << Traits::com1)|(uint8_t(o)&1 << Traits::com0) | uint8_t(c);
	}
};

struct timer2_traits
{
	static uint8_t const ocr_addr = 0x23;
	static uint8_t const tccr_addr = 0x25;
	static uint8_t const wgm0 = WGM20;
	static uint8_t const wgm1 = WGM21;
	static uint8_t const com0 = WGM20;
	static uint8_t const com1 = WGM20;
};

using fast_pwm2 = basic_fast_pwm<timer2_traits>;

}  // avro
