// pouzitie pwm rezimu casovaca 2 (postupne stmievanie ledky na OC2)
#include <util/delay.h>
#include "pwm.hpp"

using namespace avro;

int main(void)
{
	DDRB |= (1 << PB3);  // make PB3(OC2) pin output
	fast_pwm2 pwm(timer_clock_select::clk, fast_pwm_output_mode::clear_on_match);

	while (1)
	{
		for (uint8_t brightness = 0; brightness < 255; ++brightness)
		{
			pwm.compare_value = brightness;
			_delay_ms(10);
		}

		for (uint8_t brightness = 255; brightness > 0; --brightness)
		{
			pwm.compare_value = brightness;
			_delay_ms(10);
		}
	}

	return 0;
}
