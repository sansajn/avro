// specified by 'Itanium C++ ABI'
#include <cstdint>
 
// 3.3.2 One-time Construction API

extern "C" int __cxa_guard_acquire(int64_t * guard_object);
extern "C" void __cxa_guard_release(int64_t * guard_object);
extern "C" void __cxa_guard_abort(int64_t * guard_object);

int __cxa_guard_acquire(int64_t * guard_object)
{
	// Prvy bajt guard_object ma hodnotu 1 ak bol objekt uz inicializovany (pozri __cxa_guard_release), inak 0.
	return (*(uint8_t *)guard_object) == 1 ? 0 : 1;
}

void __cxa_guard_release(int64_t * guard_object)
{
	(*(uint8_t *)guard_object) = 1;
}

void __cxa_guard_abort(int64_t * guard_object)
{}
