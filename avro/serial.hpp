#pragma once
#include <cstdint>
#include <cstddef>
#include <avr/io.h>
#include <avr/interrupt.h>

/*! serial input
B transmission speed in Bauds (9600, ...) */
template<uint32_t B>
class basic_iserial
{
public:
	using size_type = uint8_t;

	~basic_iserial();
	void open();
	bool is_open() const;
	void close();
	uint8_t get();
	void read(void * buf, size_type count);
	size_type readsome(void * buf, size_type count);
	size_type gcount() const;

protected:
	size_type _count = 0;
};

//! serial output
template<uint32_t B>
class basic_oserial
{
public:
	using size_type = uint8_t;

	~basic_oserial();
	void open();
	bool is_open() const;
	void close();
	void put(uint8_t c) {}
	void write(void * buf, size_type count) {}
};

//! unbuffered serial input
template<uint32_t B>
class unbuf_iserial : public basic_iserial<B>
{
public:
	using base = basic_iserial<B>;
	using size_type = typename base::size_type;

	uint8_t get();
	void read(void * buf, size_type count);
	size_type readsome(void * buf, size_type count);
};

//! compact ring buffer implementation
template<size_t N>
class ringbuf
{
public:
	bool empty() const {return _size == 0;}
	bool full() const {return _size == N;}
	uint8_t front() {return _buf[_head];}

	void pop_front() {
		_head = (_head < (N-1)) ? _head+1 : 0;
		--_size;
	}

	void push_back(uint8_t c) {
		_buf[_tail] = c;
		_tail = (_tail < (N-1)) ? _tail+1 : 0;
		++_size;
	}

private:
	volatile uint8_t _head = 0;
	volatile uint8_t _tail = 0;
	volatile uint8_t _size = 0;
	volatile uint8_t _buf[N];
};

//! buffered serial input
template<size_t B, size_t N>
class iserial : public basic_iserial<B>
{
public:
	using base = basic_iserial<B>;
	using size_type = typename base::size_type;
	using buffer_type = ringbuf<N>;

	void open();
	void close();
	uint8_t get();
	bool get(uint8_t & c);  //!< non blocking get
	void read(void * buf, size_type count);
	size_type readsome(void * buf, size_type count);

	static void rx_isr();
	static buffer_type rxbuf;  // receiver buffer
};

template<size_t B, size_t N>
typename iserial<B, N>::buffer_type iserial<B, N>::rxbuf;

//! unbuffered serial output
template<uint32_t B>
class unbuf_oserial : public basic_oserial<B>
{
public:
	using size_type = typename basic_oserial<B>::size_type;

	void put(uint8_t c);
	void write(void * buf, size_type count);
};

/*! buffered serial output
B transmission speed in Bauds (9600, ...), N buffer size in elements. */
template<size_t B, size_t N>
class oserial : public basic_oserial<B>
{
public:
	using base = basic_oserial<B>;
	using size_type = typename base::size_type;
	using buffer_type = ringbuf<N>;

	void open();
	void close();
	void put(uint8_t c);
	void write(void * buf, size_type count);
	// writesome

	static void udre_isr();
	static buffer_type txbuf;
};

template<size_t B, size_t N>
typename oserial<B, N>::buffer_type oserial<B, N>::txbuf;

#define install_iserial(name, Baud, N) \
	using iserial_type = iserial<Baud, N>; \
	iserial_type name; \
	ISR(USART_RX_vect) { \
		iserial_type::rx_isr(); \
	}

#define install_oserial(name, Baud, N) \
	using oserial_type = oserial<Baud, N>; \
	oserial_type name; \
	ISR(USART_UDRE_vect) { \
		oserial_type::udre_isr(); \
	}

// usart helpers
inline bool frame_error_occur() {return bit_is_set(UCSR0A, FE0);}
inline void receiver_enable() {UCSR0B |= (1 << RXEN0);}
inline void transmitter_enable() {UCSR0B |= (1 << TXEN0);}
inline void receiver_disable() {UCSR0B &= ~(1 << RXEN0);}
inline void transmitter_disable() {UCSR0B &= ~(1 << TXEN0);}


template<uint32_t B>
basic_iserial<B>::~basic_iserial()
{
	close();
}

template<uint32_t B>
void basic_iserial<B>::open()
{
	constexpr uint16_t ubrrval = F_CPU/(16*B)-1;
	UBRR0H = ubrrval >> 8;
	UBRR0L = ubrrval;
	UCSR0C |= (1 << UCSZ01)|(1 << UCSZ00);  // 8 data, 1 stop
	receiver_enable();
}

template<uint32_t B>
bool basic_iserial<B>::is_open() const
{
	return bit_is_set(UCSR0B, RXEN0);
}

template<uint32_t B>
void basic_iserial<B>::close()
{
	receiver_disable();
}

template<uint32_t B>
uint8_t basic_iserial<B>::get()
{
	return -1;
}

template<uint32_t B>
void basic_iserial<B>::read(void * buf, size_type count)
{}

template<uint32_t B>
typename basic_iserial<B>::size_type basic_iserial<B>::readsome(void * buf, size_type count)
{
	return 0;
}

template<uint32_t B>
typename basic_iserial<B>::size_type basic_iserial<B>::gcount() const
{
	return _count;
}

template<uint32_t B>
basic_oserial<B>::~basic_oserial()
{
	close();
}

template<uint32_t B>
void basic_oserial<B>::open()
{
	constexpr uint16_t ubrrval = F_CPU/(16*B)-1;
	UBRR0H = ubrrval >> 8;
	UBRR0L = ubrrval;
	UCSR0C |= (1 << UCSZ01)|(1 << UCSZ00);  // 8 data, 1 stop
	transmitter_enable();
}

template<uint32_t B>
bool basic_oserial<B>::is_open() const
{
	return bit_is_set(UCSR0B, TXEN0);
}

template<uint32_t B>
void basic_oserial<B>::close()
{
	transmitter_disable();
}

template<uint32_t B>
uint8_t unbuf_iserial<B>::get()
{
	loop_until_bit_is_set(UCSR0A, RXC0);
	return UDR0;
}

template<uint32_t B>
void unbuf_oserial<B>::put(uint8_t c)
{
	loop_until_bit_is_set(UCSR0A, UDRE0);  // wait for transmitter
	UDR0 = c;
}

template<uint32_t B>
void unbuf_oserial<B>::write(void * buf, size_type count)
{
	uint8_t * p = static_cast<uint8_t *>(buf);
	for (; count; --count, ++p)
		put(*p);
}

template<uint32_t B>
void unbuf_iserial<B>::read(void * buf, size_type count)
{
	uint8_t * p = (uint8_t *)buf;
	base::_count = count;
	for (; count; --count, ++p)
		*p = get();
}

template<uint32_t B>
typename unbuf_iserial<B>::size_type unbuf_iserial<B>::readsome(void * buf, size_type count)
{
	if (count == 0 && bit_is_set(UCSR0A, RXC0))
		return 0;

	base::_count = 1;
	*((uint8_t *)buf) = UDR0;
	return 1;
}

template<size_t B, size_t N>
void iserial<B, N>::rx_isr()
{
	if (!frame_error_occur())
	{
		uint8_t c = UDR0;
		if (!rxbuf.full())  // ignore new data if full
			rxbuf.push_back(c);
	}
	else
		UCSR0A &= ~(1 << RXC0);  // clear receive complete
}

template<size_t B, size_t N>
void iserial<B, N>::open()
{
	base::open();
	UCSR0B |= (1 << RXCIE0);  // enable receiver interrupt
	sei();
}

template<size_t B, size_t N>
void iserial<B, N>::close()
{
	UCSR0B &= ~(1 << RXCIE0);  // disable receiver interrupt
	base::close();
}

template<size_t B, size_t N>
uint8_t iserial<B, N>::get()
{
	while (rxbuf.empty())
		continue;

	uint8_t c = rxbuf.front();
	rxbuf.pop_front();
	return c;
}

template<size_t B, size_t N>
bool iserial<B, N>::get(uint8_t & c)
{
	if (!rxbuf.empty())
	{
		c = rxbuf.front();
		rxbuf.pop_front();
		return true;
	}
	else
		return false;
}

template<size_t B, size_t N>
void iserial<B, N>::read(void * buf, size_type count)
{
	uint8_t * p = static_cast<uint8_t *>(buf);
	for (base::_count = 0; base::_count < count; ++p, ++base::_count)
	{
		if (!rxbuf.empty())
		{
			*p = rxbuf.front();
			rxbuf.pop_front();
		}
		else
			*p = get();
	}
}

template<size_t B, size_t N>
typename iserial<B, N>::size_type iserial<B, N>::readsome(void * buf, size_type count)
{
	uint8_t * p = static_cast<uint8_t *>(buf);
	for (base::_count = 0; base::_count < count; ++p, ++base::_count)
	{
		if (!rxbuf.empty())
		{
			*p = rxbuf.front();
			rxbuf.pop_front();
		}
		else
			break;
	}
	return base::_count;
}

template<size_t B, size_t N>
void oserial<B, N>::udre_isr()
{
	if (!txbuf.empty())
	{
		UDR0 = txbuf.front();
		txbuf.pop_front();
	}
	else
		UCSR0B &= ~(1 << UDRIE0);  // no more data to send
}

template<size_t B, size_t N>
void oserial<B, N>::open()
{
	base::open();
	UCSR0B |= (1 << UDRIE0);  // enable data empty interrupt
	sei();
}

template<size_t B, size_t N>
void oserial<B, N>::close()
{
	UCSR0B &= ~(1 << UDRIE0);  // disable data empty interrupt
	base::close();
}

template<size_t B, size_t N>
void oserial<B, N>::put(uint8_t c)
{
	while (txbuf.full())
		continue;
	txbuf.push_back(c);
	UCSR0B |= (1 << UDRIE0);  // initiate transmission
}

template<size_t B, size_t N>
void oserial<B, N>::write(void * buf, size_type count)
{
	uint8_t * p = static_cast<uint8_t *>(buf);

	while (count--)
	{
		if (!txbuf.full())  // TODO: nechcem checkovat volanim full() pri kazdom vlozeni
		{
			txbuf.push_back(*p++);
			if (bit_is_clear(UCSR0B, UDRIE0))
				UCSR0B |= (1 << UDRIE0);  // initiate transmission
		}
		else
		{
			while (txbuf.full())
				continue;
			txbuf.push_back(*p++);
		}
	}
}
