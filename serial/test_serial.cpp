#include "serial.hpp"

install_iserial(serin, 57600, 16)
install_oserial(serout, 57600, 16)

int main(int argc, char * argv[])
{
	serin.open();
	serout.open();

	serout.write((void *)"welcome to the jamaica and happy new year\n", 42);

	while (1) {
		serout.put(serin.get());
	}

	return 0;
}
