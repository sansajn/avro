#include "serial.hpp"

constexpr uint32_t BAUD = 57600;

int main(int argc, char * argv[])
{
	unbuf_iserial<BAUD> serin;
	serin.open();

	unbuf_oserial<BAUD> serout;
	serout.open();

	serout.write((void *)"welcome to the jamaica and happy new year\n", 42);

	while (1) {
		serout.put(serin.get());
	}

	return 0;
}
