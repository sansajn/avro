#include "pca.h"

int main(int argc, char const * argv[])
{
	serial_init(E_BAUD_57600);
	serial_install_interrupts(E_FLAGS_SERIAL_RX_INTERRUPT);
	serial_flush();

	while (1)
	{
		uint8_t c;
		if (!serial_getc(&c))  // non blocking call
			continue;

		serial_poll_sendc(c);
	}

	return 0;
}

