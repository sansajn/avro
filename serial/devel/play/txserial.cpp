// zakladny princip neblokovacieho posielania dat cez uart
#include <string.h>
#include <avr/interrupt.h>

constexpr uint32_t baud = 57600;
constexpr uint16_t ubrrval = F_CPU/(16*baud) - 1;

uint8_t __txbuf[10];
uint8_t __txlen = 0;
uint8_t __txidx = 0;

ISR(USART_UDRE_vect) 
{
	if (__txidx < __txlen)
		UDR0 = __txbuf[__txidx++];
	else
	{
		__txidx = 0;
		UCSR0B &= ~(1 << UDRIE0);  // no new data to send
	}
}

int main(int argc, char * argv[])
{
	UBRR0H = ubrrval >> 8;
	UBRR0L = ubrrval;
	UCSR0C |= (1 << UCSZ01)|(1 << UCSZ00);  // 8 data, 1 stop
	UCSR0B |= (1 << TXEN0);  // enable transmitter
	sei();

	strcpy((char *)__txbuf, "welcome\n");
	__txlen = 8;

	UCSR0B |= (1 << UDRIE0);  // initiate transmission

	while (1)
		continue;

	return 0;
}