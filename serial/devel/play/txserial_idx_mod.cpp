// posielanie dat cez uart v neblokovacom rezime
#include <string.h>
#include <avr/interrupt.h>

constexpr uint32_t baud = 57600;
constexpr uint16_t ubrrval = F_CPU/(16*baud) - 1;

template<size_t N>
struct ringbuf
{
	ringbuf() : _head{0}, _tail{0} {}
	bool empty() const {return _head == _tail;}
	uint8_t front() {return _buf[_head];}	
	void pop_front() {_head = _head < (N-1) ? ++_head : 0;}
	
	bool full() const {
		if (_head == 0)
			return _tail == (N-1);
		else
			return (_head-1) == _tail;
	}
	
	void push_back(uint8_t c) {
		_buf[_tail] = c;
		_tail = _tail < (N-1) ? ++_tail : 0;
	}

	volatile uint8_t _head;
	volatile uint8_t _tail;
	volatile uint8_t _buf[N];
};

ringbuf<10> __txbuf;

ISR(USART_UDRE_vect) 
{
	if (!__txbuf.empty())
	{
		UDR0 = __txbuf.front();
		__txbuf.pop_front();
	}
	else
		UCSR0B &= ~(1 << UDRIE0);  // no more data to send
}

void write(uint8_t * data, uint8_t count)
{
	while (count--)
	{
		if (!__txbuf.full())
		{
			__txbuf.push_back(*data++);
			if (bit_is_clear(UCSR0B, UDRIE0))
				UCSR0B |= (1 << UDRIE0);  // initiate transmission
		}
		else
		{
			while (__txbuf.full())
				continue;
			__txbuf.push_back(*data++);
		}
	}
}

int main(int argc, char * argv[])
{
	UBRR0H = ubrrval >> 8;
	UBRR0L = ubrrval;
	UCSR0C |= (1 << UCSZ01)|(1 << UCSZ00);  // 8 data, 1 stop
	UCSR0B |= (1 << TXEN0);  // enable transmitter
	sei();

	write((uint8_t *)"welcome to the jamaica and happy new year\n", 42);

	while (1)
		continue;

	return 0;
}
