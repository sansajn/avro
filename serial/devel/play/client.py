# verzia pre python3 nefunguj, sustaj python test_serial.py
import serial, time

ser = serial.Serial('/dev/ttyUSB0', 57600, timeout=5)
time.sleep(2)  # arduino is restarted by hw, wait (1s is not enougth)
print('port:%s' % ser.name)

# program introduction
byte_count = ser.inWaiting()
buf = ser.read(byte_count)
print('%d:%s' % (byte_count, buf))

s = 'Teresa Lisbon!'
print('writing ...')
ser.write(s)
print('transmited:%s' % s)

time.sleep(1)  # wait for ardiuno answer

print('reading ...')
byte_count = ser.inWaiting()
buf = ser.read(byte_count)
print('received:%s' % buf)

ser.close()
