// pouzitie pwm rezimu casovaca 2 (postupne stmievanie ledky na OC2)
#include <util/delay.h>
#include "pwm.hpp"

using namespace avro;

int main(void)
{
	DDRB |= (1 << PB3);  // make PB3(OC2) pin output

	fast_pwm2 pwm(timer_clock_select::clk, fast_pwm_output_mode::clear_on_match);
	pwm.compare_value = 200;  // duty-cycle : 200/255=78%

	while (1)
	{}

	return 0;
}
